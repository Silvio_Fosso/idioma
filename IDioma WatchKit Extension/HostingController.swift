//
//  HostingController.swift
//  IDioma WatchKit Extension
//
//  Created by Pierluigi Rizzu on 17/01/2020.
//  Copyright © 2020 Pierluigi Rizzu. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class HostingController: WKHostingController<ContentView> {
    override var body: ContentView {
        return ContentView()
    }
}
