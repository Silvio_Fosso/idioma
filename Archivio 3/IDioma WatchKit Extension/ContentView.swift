//
//  ContentView.swift
//  IDioma WatchKit Extension
//
//  Created by Pierluigi Rizzu on 17/01/2020.
//  Copyright © 2020 Pierluigi Rizzu. All rights reserved.
//

import SwiftUI
import Combine
struct ContentView: View {
    @State var lingua: String = ""
     @State var scrollAmount = 0.0
    @State var Opac = [1.0,0.1,0.1,0.1,0.1]
    @State var Lingue = ["Italian","English","Spanish","French","Russian"]
    @State var lg = "it"
    @State var focusa = true
    var body: some View {
        VStack (alignment: .center) {
            List{
                
                Group {
                NavigationLink(destination: SwiftUIView(audioRecorder: AudioRecorder(),lingua: lg)) {
                    Text(Lingue[0])
                    .opacity(Opac[0])
                }
                NavigationLink(destination: SwiftUIView(audioRecorder: AudioRecorder(),lingua: lg)) {
                
                    Text(Lingue[1])
                        .opacity(Opac[1])

                }

                    
                NavigationLink(destination: SwiftUIView(audioRecorder: AudioRecorder(),lingua: lg)) {
                    Text(Lingue[2])
                        .opacity(Opac[2])
                }
                NavigationLink(destination: SwiftUIView(audioRecorder: AudioRecorder(),lingua: lg)) {
                    Text(Lingue[3]).multilineTextAlignment(.center)
                        .opacity(Opac[3])
                  }
                NavigationLink(destination: SwiftUIView(audioRecorder: AudioRecorder(),lingua: lg)) {
                 Text(Lingue[4])
                    .opacity(Opac[4])
                    
                }
                }.focusable(focusa)
                    .digitalCrownRotation($scrollAmount, from: 1, through: 6,by: 1.0, sensitivity: .medium, isContinuous: false, isHapticFeedbackEnabled: true)
                
                
                
            }.listStyle(CarouselListStyle())
            
            
        }
        .onAppear{
            self.focusa = true
            
            }
        .onReceive(Just(scrollAmount)) { output in
           
                
            
             if(self.scrollAmount > 1 && self.scrollAmount < 2){
                              var cont = 0
                var salvaLing = ""
                var sg = ""
                               self.Opac =  [1.0,0.1,0.1,0.1,0.1]
                for a in self.Lingue {
                    if(cont == 0)
                    {
                      salvaLing = a
                        self.lg = "it"
                    }
                    if(a == "Italian"){
                        self.Lingue[0] = self.Lingue[cont]
                        self.Lingue[cont] = salvaLing
                       
                        break
                        
                    }
                    
                    cont+=1
                }
              
             
                           }else if(self.scrollAmount > 2 && self.scrollAmount < 3){
                var cont = 0
                  var salvaLing = ""
                 var sg = ""
                                 self.Opac =  [1.0,0.1,0.1,0.1,0.1]
                  for a in self.Lingue {
                      if(cont == 0)
                      {
                        salvaLing = a
                         self.lg = "en"
                      }
                      if(a == "English"){
                          self.Lingue[0] = self.Lingue[cont]
                          self.Lingue[cont] = salvaLing
                        
                          break
                          
                      }
                      
                      cont+=1
                  }
                
                           
           
                           }else if(self.scrollAmount > 3 && self.scrollAmount < 4){
                var cont = 0
                  var salvaLing = ""
                 var sg = ""
                                 self.Opac =  [1.0,0.1,0.1,0.1,0.1]
                  for a in self.Lingue {
                      if(cont == 0)
                      {
                        salvaLing = a
                         self.lg = "es"
                      }
                      if(a == "Spanish"){
                          self.Lingue[0] = self.Lingue[cont]
                          self.Lingue[cont] = salvaLing
                       
                          break
                          
                      }
                      
                      cont+=1
                  }
                            
          
                           }else if(self.scrollAmount > 4 && self.scrollAmount < 5){
                var cont = 0
                  var salvaLing = ""
                 var sg = ""
                                 self.Opac =  [1.0,0.1,0.1,0.1,0.1]
                  for a in self.Lingue {
                      if(cont == 0)
                      {
                        salvaLing = a
                       self.lg = "fr"
                      }
                      if(a == "French"){
                          self.Lingue[0] = self.Lingue[cont]
                          self.Lingue[cont] = salvaLing
                       
                          break
                          
                      }
                      
                      cont+=1
                  }
                       
                
              
                           }else if(self.scrollAmount > 5 && self.scrollAmount < 6){
                        var cont = 0
                          var salvaLing = ""
                 var sg = ""
                                         self.Opac =  [1.0,0.1,0.1,0.1,0.1]
                          for a in self.Lingue {
                              if(cont == 0)
                              {
                                salvaLing = a
                                 self.lg = "ru"
                              }
                              if(a == "Russian"){
                                  self.Lingue[0] = self.Lingue[cont]
                                  self.Lingue[cont] = salvaLing
                              
                                  break
                                  
                              }
                              
                              cont+=1
                          }
                              
                        
                
                            
                           }// Here is the answer.
           
               
//                    self.focusa = true
                
            
          
             
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}



