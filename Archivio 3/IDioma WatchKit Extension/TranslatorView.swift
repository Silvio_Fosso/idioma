//
//  SwiftUIView.swift
//  IDioma WatchKit Extension
//
//  Created by Pierluigi Rizzu on 17/01/2020.
//  Copyright © 2020 Pierluigi Rizzu. All rights reserved.
//

import SwiftUI
import AVFoundation


struct SwiftUIView: View {
 
    @ObservedObject var audioRecorder: AudioRecorder
    @State public var time : Timer!
   var lingua: String = ""
    var lingaentr : String = "it"
   @State public var txtinent = String("Testo in entrata")
    @State public var txttrdt = String("Testo tradotto")
    @State var degree = 0.0
   @State var degreever = false
    var body: some View {
        
        VStack {
            Text(txttrdt).bold()
           Divider()
            Text(txtinent).font(Font.system(size: 13.0)).foregroundColor(.gray)
            Spacer()
            if audioRecorder.recording == false {
                Button(action: {
                    self.audioRecorder.startRecording()
                }) {    //Qui ricostruisci il registratore, dovrebbe essere quasi copia e incolla.
                Text("Registra") // Aspettiamo le robe di design. Nel caso lunedi' non ci sono è na strunzat (CANCELLA QUESTO COMMENTO) fai Image("Nome immagine)
                }
            }else{
                Button(action: {
                    print(self.lingua)
                    self.audioRecorder.stopRecording()
                    self.send_audio_or_ling(audio: false, l: self.lingaentr, c: self.lingua)
                    self.send_audio_or_ling(audio: true, l: "", c: "")
                    let times = Timer.scheduledTimer(withTimeInterval: 0.2, repeats: true) { (pcc) in
                        self.fetchData { (dict, error) in
                            if(dict != nil){
                               
                                
                                self.txtinent = dict!["testo"] as! String
                                
                                self.txttrdt =  dict!["tradotto"] as! String
                            
                                let speechSynthesizer = AVSpeechSynthesizer()

                                let speechUtterance: AVSpeechUtterance = AVSpeechUtterance(string: self.txttrdt)

                                speechUtterance.rate = AVSpeechUtteranceMaximumSpeechRate / 2.0

                                speechUtterance.voice = AVSpeechSynthesisVoice(language: self.lingua)

                                speechSynthesizer.speak(speechUtterance)
                                pcc.invalidate()
                            
                            }
                            

                        }
                
                    }
                    
                }) {    //Qui ricostruisci il registratore, dovrebbe essere quasi copia e incolla.
                Text("stop") // Aspettiamo le robe di design. Nel caso lunedi' non ci sono è na strunzat (CANCELLA QUESTO COMMENTO) fai Image("Nome immagine)
                }
                
            }
           
        }.navigationBarTitle("IT- \(lingua)")
           
            .rotationEffect(.degrees(self.degree))
            .animation(.default)
            .onLongPressGesture {
                if(self.degreever == false){
                    self.degreever = true
                    self.degree = 180
                } else  if(self.degreever == true){
                    self.degreever = false
                    self.degree = 0
                }
        }
    }
    
    func fetchData(completion: @escaping ([String:Any]?, Error?) -> Void) {
        var link = "http://pxgamers.altervista.org/traduzione.php"
        let url = URL(string: link)!

        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else { return }
            do {
                if let array = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any]{
                    completion(array, nil)
                }
            } catch {
                print(error)
                completion(nil, error)
            }
        }
        task.resume()
    }

    
        
    func typeWriter(testo : String, Tradotto : String){
        let array_Of_testo = Array(testo)
        let array_Of_Tradotto = Array(Tradotto)
        var cont_testo = 0
        var cont_tradotto = 0
        var flag = false
        var flag1 = false
        self.txtinent = ""
        self.txttrdt = ""
        let timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { (prr) in
            if(cont_testo < array_Of_testo.count-1){
                self.txtinent += String(array_Of_testo[cont_testo])
                cont_testo+=1
            }else{
                flag = true
            }
            
            
            if(cont_tradotto < array_Of_Tradotto.count-1){
                self.txttrdt += String(array_Of_Tradotto[cont_tradotto])
                cont_tradotto+=1
            }else{
                flag1 = true
            }
            
            if(flag == true && flag1 == true){
                prr.invalidate()
            }
            
            
            
            
        }
        
    }


    func send_audio_or_ling(audio :Bool,l : String,c : String){
       
        var url = URL(string: "http://pxgamers.altervista.org/Audio/data.php")!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        let documentPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let audioFilename = documentPath.appendingPathComponent("Giovanni.wav")
        guard let data1 = try? Data(contentsOf:audioFilename) else { return }
        if(audio == true){
        request.httpBody = data1
        }else{
            let parameters: [String: Any] = [
                "entr": l.lowercased(),
                "fin": c
            ]
            url = URL(string: "http://pxgamers.altervista.org/Carica_Lingua.php")!
            request = URLRequest(url: url)
            request.httpBody = parameters.percentEncoded()
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            
        }
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data,
                let response = response as? HTTPURLResponse,
                error == nil else {                                              // check for fundamental networking error
                print("error", error ?? "Unknown error")
                return
            }

            guard (200 ... 299) ~= response.statusCode else {                    // check for http errors
                print("statusCode should be 2xx, but is \(response.statusCode)")
                print("response = \(response)")
                return
            }

            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(responseString)")
        }

        task.resume()
    }
}

extension CharacterSet {
    static let urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="

        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        return allowed
    }()
}
extension Dictionary {
    func percentEncoded() -> Data? {
        return map { key, value in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            return escapedKey + "=" + escapedValue
        }
        .joined(separator: "&")
        .data(using: .utf8)
    }
}

struct SwiftUIView_Previews: PreviewProvider {
    static var previews: some View {
        SwiftUIView(audioRecorder: AudioRecorder())
    }
}
