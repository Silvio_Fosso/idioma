//
//  NotificationView.swift
//  IDioma WatchKit Extension
//
//  Created by Pierluigi Rizzu on 17/01/2020.
//  Copyright © 2020 Pierluigi Rizzu. All rights reserved.
//

import SwiftUI

struct NotificationView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
